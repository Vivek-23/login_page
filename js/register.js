const firstName = document.getElementById('first_name');
const lastName = document.getElementById('last_name');
const email = document.getElementById('email');
const password = document.getElementById('password');
const form = document.getElementById('form');
const errorFirst = document.getElementById('error-first');
const errorLast = document.getElementById('error-last');
const errorEmail = document.getElementById('error-email');
const errorPassword = document.getElementById('error-password');


let validate = true;

function loginFunction() {
    form.addEventListener('submit', (err) => {
        err.preventDefault();
        validateInputs();
        if(validate){
            setTimeout(() => {
                window.location.reload();
            }, 1000)
        }else {
        }
    
    });
}

function validateInputs() {
    let emailValidate = true,passwordValidate = true,
        firstNameValidate = true,lastNameValidate = true;

    let emailId = email.value;
    let passwordId = password.value;
    let firstNameId = firstName.value;
    let lastNameId = lastName.value;

    if(passwordId.length < 8) {
        passwordValidate = false;
        errorPassword.style.visibility = 'visible';
    }
    else if(passwordId == ''){
        passwordValidate = false;
        errorPassword.style.visibility = 'visible';
    }
    else{
        errorPassword.style.visibility = 'hidden';
    }

    if(emailId == ''){
        emailValidate = false;
        errorEmail.style.visibility = 'visible';
    }
    else {
        errorEmail.style.visibility = 'hidden';
    }
    
    if(firstNameId == ''){
        firstNameValidate = false;
        errorFirst.style.visibility = 'visible';
    }
    else {
        errorFirst.style.visibility = 'hidden';
    }
    
    if(lastNameId == ''){
        lastNameValidate = false;
        errorLast.style.visibility = 'visible';
    }
    else{
        errorLast.style.visibility = 'hidden';
    }

    if(!firstNameValidate || !lastNameValidate || 
        !emailValidate || !passwordValidate){
        validate = false
    }
    else{
        validate = true;
    }
       
}
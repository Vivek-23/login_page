const email = document.getElementById('email');
const password = document.getElementById('password');
const form = document.getElementById('form');
const errorEmail = document.getElementById('error-email');
const errorPassword = document.getElementById('error-password');


let validate = true;

function loginFunction() {
    form.addEventListener('submit', (event) => {
        event.preventDefault();
        validateInputs();
        if(validate){
            setTimeout(() => {
                window.location.reload();
            }, 1000)
        }else {
        }
    
    });
}

function validateInputs() {
    let passwordValidate= true, emailValidate=true;
    let emailId = email.value;
    let passwordId = password.value;

    if(passwordId.length < 8) {
        passwordValidate = false;
        errorPassword.style.visibility = 'visible';
    }
    else if (passwordId === ''){
        passwordValidate = false;
        errorPassword.style.visibility = 'visible';
    }
    else {
        errorPassword.style.visibility = 'hidden';
    }

    if(emailId === ''){
        emailValidate = false;
        errorEmail.style.visibility = 'visible';
    }else {
        errorEmail.style.visibility = 'hidden';
    }
    

    (!passwordValidate || !emailValidate) ? 
        validate = false : validate = true;
    
       
}